# alpine-docker-registry

#### [alpine-x64-docker-registry](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-docker-registry "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-docker-registry "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)
#### [alpine-aarch64-docker-registry](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-docker-registry/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-docker-registry "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-docker-registry "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-docker-registry/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-docker-registry/)
#### [alpine-armhf-docker-registry](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-docker-registry/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-docker-registry "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-docker-registry "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-docker-registry/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-docker-registry/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : docker-registry
    - The Registry is a stateless, highly scalable server side application that stores and lets you distribute Docker images.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 5000:5000/tcp \
           -v /repo:/repo \
           forumi0721alpinex64/alpine-x64-docker-registry:latest
```

* aarch64
```sh
docker run -d \
           -p 5000:5000/tcp \
           -v /repo:/repo \
           forumi0721alpineaarch64/alpine-aarch64-docker-registry:latest
```

* armhf
```sh
docker run -d \
           -p 5000:5000/tcp \
           -v /repo:/repo \
           forumi0721alpinearmhf/alpine-armhf-docker-registry:latest
```



----------------------------------------
#### Usage

* push
```
docker push localhost:5000/<repository>
```

* pull
```
docker pull localhost:5000/<repository>
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 5000/tcp           | Listen port for reistory daemon                  |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-docker-registry](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)
* [forumi0721alpineaarch64/alpine-aarch64-docker-registry](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-docker-registry/)
* [forumi0721alpinearmhf/alpine-armhf-docker-registry](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-docker-registry/)

